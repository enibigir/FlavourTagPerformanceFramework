
mkdir -p build

cd build

cmake ..

make -j 4

source x86_64-slc6-gcc62-opt/setup.sh

cd ..